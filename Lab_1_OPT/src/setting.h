#ifndef SETTING_H
#define SETTING_H

/* SOURCE */
#define TRANSLATOR "[TRANSALTOR]"
#define LEXER      "[LEXER]"

/* PROGRAMME CODE */
#define FAILURE -1
#define SUCCESS  0

/* COLOR */
#define RESET         "\x1b[0m"
#define ERROR_COLOR   "\x1b[31;1m" // LIGHT_RED 
#define DEBUG_COLOR   "\x1b[34m"   // BLUE
#define SUCCESS_COLOR "\x1b[32;1m" // GREEN

/* STRUCTURES */

enum token_type {
	WHITESPACE,
	DIGIT,
	LETTER,
	DELIMITER,
	SHARP,
	SIGN,
	OPEN_PARENTHESIS,
	CLOSE_PARENTHESIS,
	ASTERISK,
};

typedef struct {
	int line_t;
	int  col_t;
	int code_t;
} token_t;

#endif
 