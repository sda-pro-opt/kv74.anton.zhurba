#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "symbol-table.h"
#include "setting.h"

int st_setup(SymbolTable* st, size_t capacity) {
    if (st == NULL) {
	    return ST_ERROR;
    }

    if (capacity < ST_MIN_CAPACITY) {
        capacity = ST_MIN_CAPACITY;
    }

    if (st_allocate_(st, capacity) == ST_ERROR) {
        return ST_ERROR;
    }

    st->size = 0;

    return ST_SUCCESS;
}

int st_destroy(SymbolTable* st) {
	if (!st_is_initialized(st)) {
		return ST_ERROR;
	}

	STNode* node;
	STNode* next;

	for (size_t i = 0; i < st->capacity; i += 1) {
		node = st->nodes[i];
		while(node) {
			next = node->next;
			st_destroy_node_(node);
			node = next;
		}
	}

	free(st->nodes);

	return ST_SUCCESS;
}

int st_print(SymbolTable* st) {
	if (!st_is_initialized(st)) {
		return ST_ERROR;
	}

	STNode* node;

	for (size_t i = 0; i < st->capacity; i += 1) {
		node = st->nodes[i];
		while(node) {
			if (node->value != WHITESPACE 
					&& node->value != DIGIT && node->value != LETTER) {
						printf("key: %15s value: %15d;\n", node->key, 
						node->value > 255 ? node->value : (int)(node->key[0]));
			}
			node = node->next;
		}
	}

	return ST_SUCCESS;
}

char* st_get_key(SymbolTable* st, int value) {
	if (!st_is_initialized(st)) {
		return ST_ERROR;
	}

	STNode* node;

	for (size_t i = 0; i < st->capacity; i += 1) {
		node = st->nodes[i];
		while(node) {
			if (node->value == value) {
				return node->key;
			}
			node = node->next;
		}
	}

	return NULL;
}

int st_insert(SymbolTable* st, char* key, int value) {
    if (!st_is_initialized(st)) {
	    return ST_ERROR;
    }

    if (key == NULL) {
	    return ST_ERROR;
    }

    if (st_should_grow_(st)) {
		st_resize_(st, st->size * ST_GROW_FACTOR);
    }

    size_t index = st_hash_(key) % st->capacity;
	STNode* node;

	for (node = st->nodes[index]; node; node = node->next) {
		if (strcmp(key, node->key) == 0) {
			node->value = value;
			return ST_SUCCESS;
		}
	}

	if (st_push_front_(st, index, key, value) == ST_ERROR) {
		return ST_ERROR;
	}

    st->size += 1;

	return value;
}

int st_lookup(SymbolTable* st, char* key) {
	if (!st_is_initialized(st)) {
		return ST_ERROR;
	}

	if (key == NULL) {
		return ST_ERROR;
	}

	size_t index = st_hash_(key) % st->capacity;
	STNode* node;

	for (node = st->nodes[index]; node; node = node->next) {
		if (strcmp(key, node->key) == 0) {
			return node->value;
		}
	}

	return ST_NOT_FOUND;
}

bool st_is_empty(SymbolTable* st) {
	if (st == NULL) {
		return ST_ERROR;
	}

	return st->size == 0;
}

bool st_is_initialized(SymbolTable* st) {
    return st != NULL && st->nodes != NULL;
}


int st_allocate_(SymbolTable* st, size_t capacity) {
    if ((st->nodes = malloc(capacity * sizeof(STNode*))) == NULL) {
        return ST_ERROR;
    }

    memset(st->nodes, 0, capacity * sizeof(STNode*));

    st->capacity  = capacity;
    st->threshold = capacity * ST_LOAD_FACTOR;

    return ST_SUCCESS;
}

int st_push_front_(SymbolTable* st, size_t index, char* key, int value) {
	st->nodes[index] = st_create_node_(key, value, st->nodes[index]);

	return st->nodes[index] != NULL ? ST_SUCCESS : ST_ERROR;
}

STNode* st_create_node_(char* key, int value, STNode* next) {
	STNode* node;

	if ((node = malloc(sizeof(STNode))) == NULL) {
		return NULL;
	}
	if ((node->key = malloc(strlen(key) + 1)) == NULL) {
		return NULL;
	}

	node->value = value;
	strcpy(node->key, key);
	node->next  = next;

	return node;
}

int st_resize_(SymbolTable* st, size_t new_capacity) {
	STNode** old_node     = st->nodes;
	size_t   old_capacity = st->capacity;

	if (st_allocate_(st, new_capacity) == ST_ERROR) {
		return ST_ERROR;
	}

	st_rehash_(st, old_node, old_capacity);

	free(old_node);
	return ST_SUCCESS;
}

void st_rehash_(SymbolTable* st, STNode** old_node, size_t old_capacity) {
	STNode* node;
	STNode* next;
	size_t  new_index;

	for (size_t i = 0; i < old_capacity; i += 1) {
		for (node = old_node[i]; node; ) {
			next = node->next;
			new_index = st_hash_(node->key) % st->capacity;
			node->next = st->nodes[new_index];
			st->nodes[new_index] = node;

			node = next;
		}
	}
}

size_t st_hash_(char* raw_key) {
	size_t hash = 5381;
	char*  key  = raw_key;
	int character;
	while ((character = *key++)) {
		hash = (hash + (hash << 5u)) + character;
	}
	return hash;
}

bool st_should_grow_(SymbolTable* st) {
	return st->size == st->threshold;
}

void st_destroy_node_(STNode* node) {
	if (node != NULL) {
		free(node->key);
		free(node);
	}
}
