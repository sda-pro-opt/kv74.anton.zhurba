#ifndef ERROR_H
#define ERROR_H

#include "setting.h"

#define ILLEGAL_SYMBOL   "Illegal character detected"
#define UNCLOSED_COMMENT "Unclosed comment detected"

#define ERROR(source, error) \
  printf("%s %s%s%s\n", source, ERROR_COLOR, error, RESET) \

void print_error(char* source, int line, int col, char* error);

#endif