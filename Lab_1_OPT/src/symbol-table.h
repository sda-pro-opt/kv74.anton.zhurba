#ifndef TRANSLATOR_SYMBOL_TABLE_H
#define TRANSLATOR_SYMBOL_TABLE_H

#include <stddef.h>
#include <stdbool.h>

/* DEFINITIONS */

#define ST_SUCCESS        1
#define ST_ERROR          0
#define ST_MIN_CAPACITY   8
#define ST_LOAD_FACTOR    0.75
#define ST_GROW_FACTOR    2
#define ST_NOT_FOUND     -1


/* STRUCTURES */

typedef struct STNode {
    char*   key;
    int   value;

    struct STNode* next;
} STNode;

typedef struct SymbolTable {
    size_t  capacity;
    size_t threshold;
    size_t      size;

    STNode**   nodes;
} SymbolTable;


/* INTERFACE */

int     st_setup(SymbolTable* st, size_t capacity);
int     st_destroy(SymbolTable* st);
int     st_print(SymbolTable* st);
int     st_insert(SymbolTable* st, char* key, int value);
char*   st_get_key(SymbolTable* st, int value);
int     st_lookup(SymbolTable* st, char* key);
bool    st_is_empty(SymbolTable* st);

/* PRIVATE */

bool    st_is_initialized(SymbolTable* st);
int     st_allocate_(SymbolTable *pTable, size_t capacity);
int     st_resize_(SymbolTable* st, size_t new_capacity);
void    st_rehash_(SymbolTable* st, STNode** old_node, size_t old_capacity);
size_t  st_hash_(char* raw_key);
bool    st_should_grow_(SymbolTable* st);
STNode* st_create_node_(char* key, int value, STNode* next);
int     st_push_front_(SymbolTable* st, size_t index, char* key, int value);
void    st_destroy_node_(STNode* node);

#endif
