#ifndef TRANSLATOR_UTILIES_H
#define TRANSLATOR_UTILIES_H

#include <stddef.h>
#include <stdlib.h>


#define vec_init(name, type)   \
	type* name = NULL;           \
	int __ ## name ## _p   = 0;  \
	int __ ## name ## _max = 0   \

#define vec_clear(name)   \
	__ ## name ## _p   = 0; \
	__ ## name ## _max = 0; \
	free(name); name = NULL \
	

#define vec_should_grow(name)                       \
	if (__ ## name ## _p >= __ ## name ## _max) {     \
		name = realloc(name, (__ ## name ## _max += 32) \
			* sizeof(name[0]));                           \
	}                                                 \

#define vec_size(name) \
		__ ## name ## _p   \

#define vec_append(name, ch)     \
	vec_should_grow(name)          \
	name[__ ## name ## _p++] = ch  \


#endif
