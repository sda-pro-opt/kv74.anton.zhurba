#include <stdio.h>

#include "symbol-table.h"
#include "grammar.h"
#include "utilies.h"
#include "setting.h"
#include "error.h"

static int cur_ch = ' ', line = 1, col = 0;

SymbolTable st;
FILE* input_file;

vec_init(token, char);
vec_init(lexems, token_t);

void next_character() {
	cur_ch = getc(input_file);
	col += 1;

	if (cur_ch == NEWLINE_CH) {
		line += 1; col = 0;
	}
}

void identifier(int line_t, int col_t) {
	vec_clear(token);
	int character_type;

	do {
		vec_append(token, cur_ch);
		next_character();
		character_type = st_lookup(&st, (char *) &cur_ch);
	} while (character_type == DIGIT || character_type == LETTER);

	vec_append(token, '\0');

	int code = st_lookup(&st, token);
	if (code == ST_NOT_FOUND) {
		code = st_insert(&st, token, IDENTIFIERS++);
	}

	vec_append(lexems, ((token_t){
			line_t, col_t, code,
	}));
}

void comment(int line_t, int col_t) {
	int character_type = st_lookup(&st, (char *) &cur_ch);

	if (character_type != ASTERISK)
		return print_error(LEXER, line_t, col_t, ILLEGAL_SYMBOL);

	for(;;) {
		next_character();
		while (st_lookup(&st, (char *) &cur_ch) == ASTERISK) {
			next_character();
			if (st_lookup(&st, (char *) &cur_ch) == CLOSE_PARENTHESIS) {
				next_character();
				return;
			}
		}
		if (cur_ch == EOF) {
			return print_error(LEXER, line_t, col_t, UNCLOSED_COMMENT);
		}
	}
}

int digit_() {
	if (st_lookup(&st, (char *) &cur_ch) == SIGN) {
		vec_append(token, cur_ch);
		next_character();
		if (st_lookup(&st, (char *) &cur_ch) != DIGIT) {
			print_error(LEXER, line, col, ILLEGAL_SYMBOL);
		}
	}
	do {
		vec_append(token, cur_ch);
		next_character();
		if (st_lookup(&st, (char *) &cur_ch) == SHARP) {

			return SHARP;
		}
	} while (st_lookup(&st, (char *) &cur_ch) == DIGIT);

	return st_lookup(&st, (char *) &cur_ch);
}

void digit(int line_t, int col_t) {
	vec_clear(token);

	int character_type;
	if ((character_type = digit_()) == SHARP) {
		vec_append(token, cur_ch);
		next_character();
		character_type = st_lookup(&st, (char *) &cur_ch);
		if (character_type != SIGN && character_type != DIGIT) {
			return print_error(LEXER, line, col, ILLEGAL_SYMBOL);
		}
		else {
			character_type = digit_();
		}
	}

	if(character_type != DELIMITER && character_type != WHITESPACE) {
		return print_error(LEXER, line, col, ILLEGAL_SYMBOL);
	}

	vec_append(token, '\0');
	int code = st_lookup(&st, token);
	if (code == ST_NOT_FOUND) {
		code = st_insert(&st, token, CONSTS++);
	}

	vec_append(lexems, ((token_t){
			line_t, col_t, code,
	}));
}

void get_token() {
	while(st_lookup(&st, (char *) &cur_ch) == WHITESPACE) {
		next_character();
	}

	if (cur_ch == EOF) return;

	int line_t = line, col_t = col;
	switch (st_lookup(&st, (char *) &cur_ch)) {
		case LETTER:
			identifier(line, col);
			break;
		case  SIGN:
		case DIGIT:
			digit(line_t, col_t);
			break;
		case OPEN_PARENTHESIS:
			next_character();
			comment(line_t, col_t);
			break;
		case DELIMITER:
		vec_append(lexems, ((token_t){
				line,
				col,
				(int)cur_ch
		}));
			next_character();
			break;
		default:
			print_error(LEXER, line_t, col_t, ILLEGAL_SYMBOL);
	}
}

token_t* lexical_analyse() {
	do {
		get_token();
	} while(cur_ch != EOF);

	return lexems;
}



