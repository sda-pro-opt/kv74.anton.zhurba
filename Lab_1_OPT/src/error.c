#include "error.h"

#include "setting.h"
#include <stdlib.h>
#include <stdio.h>

FILE* output_file;
FILE* input_file;

void print_error(char* source, int line, int col, char* error) {
  
  printf("%s%s (line %d:col %d) error: %s%s\n", source, ERROR_COLOR, line, col, error, RESET);
  fprintf(output_file, "%s (line %d:col %d) error: %s\n", source,  line, col, error);
  
  fclose(input_file);
  fclose(output_file);
  exit(FAILURE);
}
