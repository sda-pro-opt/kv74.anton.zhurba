#ifndef GRAMMAR_H
#define GRAMMAR_H

#include "symbol-table.h"
#include "setting.h"

static int KEYWORDS    = 301;
static int IDENTIFIERS = 401;
static int CONSTS      = 501;


/* DEFINITIONS */
#define __INIT_WHITESPACE(st)       \
	st_insert(st, " ",  WHITESPACE);  \
	st_insert(st, "\t", WHITESPACE);  \
	st_insert(st, "\n", WHITESPACE);  \
	st_insert(st, "\v", WHITESPACE);  \
	st_insert(st, "\r", WHITESPACE);  \

#ifdef __APPLE__
    #define NEWLINE_CH '\r'
#else
    #define NEWLINE_CH '\n'
#endif

#define __INIT_NUMBER(st)                   \
	for (int c = '1'; c <= '9'; c += 1) {     \
        st_insert(st, (char *) &c, DIGIT);  \
	}                                         \

#define __INIT_LETTER(st)                   \
	for (int c = 'a'; c <= 'z'; c += 1) {     \
        st_insert(st, (char *) &c, LETTER); \
	}                                         \
	for (int c = 'A'; c <= 'Z'; c += 1) {     \
        st_insert(st, (char *) &c, LETTER); \
	}                                         \

#define __INIT_KEYWORD(st)                 \
	st_insert(st, "PROGRAM", KEYWORDS++);    \
	st_insert(st, "BEGIN",   KEYWORDS++);    \
	st_insert(st, "CONST",   KEYWORDS++);    \
	st_insert(st, "END",     KEYWORDS++);    \

#define __INIT_DELIMITERS(st)              \
	st_insert(st, ".",  DELIMITER);          \
	st_insert(st, ";",  DELIMITER);          \
	st_insert(st, "=",  DELIMITER);          \
	st_insert(st, "#",  SHARP);              \

#define __INIT_SIGN(st)	                   \
	st_insert(st, "-",  SIGN);               \
	st_insert(st, "+",  SIGN);               \

#define __INIT_COMMENT(st)                 \
	st_insert(st, "(",  OPEN_PARENTHESIS);   \
	st_insert(st, ")",  CLOSE_PARENTHESIS);  \
	st_insert(st, "*",  ASTERISK);           \


#define INIT_GRAMMAR(st) \
	__INIT_NUMBER(st);     \
	__INIT_LETTER(st);     \
	__INIT_KEYWORD(st);    \
	__INIT_DELIMITERS(st); \
	__INIT_WHITESPACE(st); \
	__INIT_SIGN(st);       \
	__INIT_COMMENT(st);    \

#endif
