#include "error.h"
#include "setting.h"
#include "symbol-table.h"
#include "grammar.h"
#include "lexer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern FILE* input_file;
extern FILE* output_file;
extern SymbolTable st;



int load_input(const char* file_name) {
  if (!(input_file = fopen(file_name, "r"))) {
    ERROR(TRANSLATOR, "No such file or directory");
    return EXIT_FAILURE;
  }
  
  #ifdef DEBUG
    printf("%sProcessing file - %s%s\n", DEBUG_COLOR, file_name, RESET);
  #endif

  return SUCCESS;
}

void load_output(const char* file_name) {
  int pos = 0;
	for (int i = 0; i < strlen(file_name); i += 1) {
		if (file_name[i] == '/') pos = i;
	}
	char path[256];
	strcpy(path, file_name); path[pos] = '\0';
	output_file = fopen(strcat(path, "/generate.txt"), "w+");
}

void generate_output(token_t* lexems) {
	while (lexems->code_t != 0) {
		fprintf(output_file, "%10s%5d%5d%5d\n", 
    (lexems->code_t > 255 ? st_get_key(&st, lexems->code_t) : (char*)&lexems->code_t),
    lexems->code_t, lexems->line_t, lexems->col_t);
		++lexems;
	}
	fclose(output_file);
}

int main(int argc, char** argv) {
  #ifdef DEBUG
    printf("%sDEBUG MODE ENABLED%s\n", DEBUG_COLOR, RESET);
  #endif

  if (argc < 2) {
    ERROR(TRANSLATOR, "No input file name,supply it on the command line");
    exit(FAILURE);
  }

  if (load_input(*++argv) == FAILURE) {
    exit(FAILURE);
  }

  load_output(*argv);

  st_setup(&st, 200);
  INIT_GRAMMAR(&st);

  #ifdef DEBUG
    printf("%sGrammar init: %s\n", DEBUG_COLOR, RESET);
    st_print(&st);
  #endif

  token_t* lexems = lexical_analyse();
  
  generate_output(lexems);

  st_destroy(&st);
  fclose(input_file);
}

