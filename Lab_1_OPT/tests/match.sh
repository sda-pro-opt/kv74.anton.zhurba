#!/bin/bash


DIFF=$(diff $1/generate.txt $1/expected.txt)
if [ "$DIFF" == "" ];
then
  echo -e "\x1b[90mFile: \x1b[0m\x1b[1m$1\x1b[0m\x1b[90m, Result: \x1b[32;1m PASSED\x1b[0m"
else
  echo -e "\x1b[90mFile: \x1b[0m\x1b[1m$1\x1b[0m\x1b[90m, Result: \x1b[31;1m FAILED\x1b[0m"
fi;