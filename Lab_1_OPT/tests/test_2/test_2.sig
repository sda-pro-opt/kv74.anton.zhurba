(*
  All lexical independent tokens - not syntactically correct
*)

(* PROGRAM *)  PROGRAM   (* CONST *)  123 
(* PROGRAM *)  BEGIN     (* CONST *)  -123
(* PROGRAM *)  CONST     (* CONST *)  -123#12
(* PROGRAM *)  END       (* CONST *)  -123#-13
(* DELIMITER *)  .       (* CONST *)   123#-13
(* DELIMITER *)  ;       (* IDENTIFIER *) VAL
(* DELIMITER *)  =       (* COMMENT    *) (**)

